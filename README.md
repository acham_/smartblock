SmartBlock
==========
Alexis Champsaur

Georgia Tech and Sandia Labs, October 2016

SmartBlock is an approach for building
HPC workflows "out of the box" and launching
them as single jobs, all through
traditional launch scripts for supercomputers.

SmartBlock includes a number of existing
generic components, whose code is in:
`src/generic_components/`

This work has been published in IPDRM@IPDPS 2017:
https://doi.org/10.1109/IPDPSW.2017.149

This work was funded by a grant from the US Department of Energy, Office of Science under the guidance of Lucy Nowell for the Data Management grant program.

Abstract:

Multi-step scientific workflows have become prominent and powerful tools of
data-driven scientific discovery. Run-time analytic techniques are now commonly
used to mitigate the performance effects of using parallel file systems as
staging areas during workflow execution. However, workflow construction and
deployment for extreme-scale computing is still largely an ad hoc process with
uneven support from existing tools. In this paper, we present SMARTBLOCK, an
approach to designing generic, reusable components for end-to-end construction
of workflows. Specifically, we demonstrate that a small set of SMARTBLOCK
generic components can be reused to build a diverse set of workflows, using
examples based on actual analytic processes with three well-known scientific
codes. Our evaluation shows promising scaling properties as well as negligible
overheads for using a modular approach over a custom, "all-in-one" solution. As
extreme-scale systems incorporate data analytics on simulation data as it is
generated at rates that far outstrip available I/O bandwidth, tools such as
SMARTBLOCK will become increasingly valuable for defining and deploying
flexible, efficient workflows.

## Building
The main dependencies of SmartBlock are:

* [EVPath](http://www.cc.gatech.edu/systems/projects/EVPath)
* [ADIOS](https://www.olcf.ornl.gov/center-projects/adios). Note that
ADIOS needs to be installed with FlexPath. Point FlexPath flag
(`--with-flexpath=`)
to the EVPath install directory.

To build, set the appropriate variables in
the `Makefile` in `src/generic_components/`.

* Set `ADIOS_DIR` to the install path for ADIOS.
* Set `OUTROOT` to desired install directory in the Makefile.
* Set `-DENABLE_MONITOR` in the `CPPFLAGS` in the Makefile
to enable the monitoring library (timing and memory usage measurements)

Then run `make`.
Executables will be in `${OUTROOT}/bin/`.

## Running
Example of how SmartBlock workflows are launched can be seen
in `workflows/sg/`.
A particular example of a LAMMPS-based workflow
launched on a cluster without a scheduler
can be seen in: `workflows/sg/lammps/wflaunch.sh`.

## Code Changes to Simulations
To allow simulations to work with SmartBlock, they must
be modified to include the ADIOS interface.
An example of how this is done can be seen in the modified
GROMACS source code in `src/gromacs-4.6.7`, particularly in
files `src/gromacs-4.6.7/kernel/mdrun.c` and
`src/gromacs-4.6.7/kernel/md.c`.

## Generic Components
### Select
This program is designed to be an intermediary component
in a scientific workflow. It selects certain indices from one 
of the dimensions of a multi-dimensional array read as input from
a Flexpath stream. The indices to be selected are specified by their
names, which are recognized using a header string that is part  of 
the input.
The output is another multi-dimensional array consisting of the 
selected quantities.

The names of the input and output streams and arrays, as well as the 
quantities to be extracted are passed as command-line arguments.

The input stream must contain a header called `header_string` to 
indicate what quantities are contained in the input array, and how they
are arranged. The format of the header string is `var1:var2:var3...`
where `var1`, `var2`, `var3` ... are the names of the quantities in the input
array, in the order in which they appear there.

Quantities required in the input stream:

 - zero-terminated byte array named `header_string`
    (the header string, a colon-separated list of the names of
     the variables in the dimension to be selected from)
 - integer named `header_len` (length of the above, including terminator)
 - `uint64_t` (adios unsigned long) named `ntimestep` (time step- no major 
  role in the selection)
 - data array

Usage:
```
 <exec> input-stream-name input-array-name dimension-index \
        output-stream-name output-arr-name \
        arg1 [arg2] [arg3] ...
```
Where `arg1`, `arg2` ... are the names of the quantities to be selected
from the dimension whose index in the order of dimensions (`dim0`, `dim1` ...)
is dimensions-index.
(`dim0`, `dim1` ...) is the order in which the dimensions of the input 
array are represented in the input stream.

### Magnitude
This program is designed to be an intermediary component
in a scientific workflow. Given as input a 2D array composed
of a quantity broken into its N dimensionsal components over 
any number of datapoints and timesteps, this program computes 
and outputs the magnitudes for this quantity 
for all data points into a new 1-D array for each timestep, 
through a new Flexpath stream.

Input and output is done through ADIOS/FLEXPATH.

* INPUT: 2D array in a flexpath stream
* OUTPUT: 1D array in a flexpath stream

```
Example input: 
   [v1_x, v1_y, v1_z, v2_x, v2_y, v2_z, ... ]
Example output:
   [ |v1| , |v2| , ... ]
```

Quantities required in the input stream:

 - zero-terminated byte array `header_string` (just to keep track
        of what quantities are being used to calculate the magnitude
	- not crucial to calculations)
 - integer named `header_len` (length of the above)
 - `uint64_t` (adios unsigned long) named `ntimestep`

Input array dimensions: 1st dimension should span the data "points," such
as the particles. 2nd dimension should span the components of the desired
magnitude for each point.


Usage:

```
<exec> input-stream-name input-arr-name \
        output-stream-name ouput-arr-name 
```

### Dim-Reduce

This program is designed to be an intermediary component
in a scientific workflow. 

Usage:
```
<exec> input-stream-name input-array-name 
        dim-to-remove dim-to-grow
        output-stream-name output-array-name
```
Given a multi-dim array, the dimension whose index is `dim-to-grow` 
will "absorb" the dimension whose index is `dim-to-remove`.



### Histogram
Histogram Component
by
Venkatram Vishwanath, Argonne Labs and 
Alexis Champsaur, Sandia Labs, Georgia Tech.

This program is designed to be an enpoint component in 
a scientific workflow. It reads a 1-D array from a data
stream using ADIOS-FLEXPATH, and computes and writes to a disk file
a histogram of the values contained in the array for each timestep 
until the end of the stream is reached.

Usage:
```
<exec> input-stream-name input-array-name num-bins
  (where num-bins is the number of bins of the final histograms)
```

Quantities required in the input stream:

 - global 1D array containing data of interest for the histogram
 - `uint64_t` (adios unsigned long) named `ntimestep`.
