#!/bin/bash
#PBS -A CSC094
#PBS -N gtcp
#PBS -j oe
#PBS -V
#PBS -l walltime=00:12:00
#PBS -l nodes=8

#export CMTransportVerbose=1
#export CMTraceFile=1
export CMSelfFormats=1
#export CMTransport="enet"
cd $GTCPRUN

aprun -n 64 $GTCPEXEC/bench_gtc_titan_gencomps D2.txt 1 2 &> gtc.out &
aprun -n 10 $GEN/install/bin/select gtc_raw.fp moments 2 gtc_select.fp \
perp_pressures perp_press &> select.out &
aprun -n 6 $GEN/install/bin/dimreduce gtc_select.fp perp_pressures 2 1 \
twoDpress.fp twoDpress &> dimred1.out &
aprun -n 6 $GEN/install/bin/dimreduce2 twoDpress.fp twoDpress 0 1 \
oneDpress.fp oneDpress &> dimred2.out &
aprun -n 2 $GEN/install/bin/histogram oneDpress.fp 32 oneDpress &> histo.out &

#aprun -n 8 $GTCPEXEC/bench_gtc_titan_gencomps A.txt 1 2 &> gtc.out &
#aprun -n 2 $GEN/install/bin/select gtc_raw.fp moments 2 gtc_select.fp \
#perp_pressures perp_press &> select.out &
#aprun -n 2 $GEN/install/bin/dimreduce gtc_select.fp perp_pressures 2 1 \
#twoDpress.fp twoDpress &> dimred1.out &
#aprun -n 2 $GEN/install/bin/dimreduce2 twoDpress.fp twoDpress 0 1 \
#oneDpress.fp oneDpress &> dimred2.out &
#aprun -n 2 $GEN/install/bin/histogram oneDpress.fp 16 oneDpress &

wait
