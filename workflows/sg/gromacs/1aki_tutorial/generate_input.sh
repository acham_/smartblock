#!/bin/bash

#pdb2gmx_mpi -f 1AKI.pdb -o 1AKI_processed.gro -water spce
#editconf_mpi -f 1AKI_processed.gro -o 1AKI_newbox.gro -c -d 1.0 -bt cubic
#genbox_mpi -cp 1AKI_newbox.gro -cs spc216.gro -o 1AKI_solv.gro -p topol.top
#grompp_mpi -f ions.mdp -c 1AKI_solv.gro -p topol.top -o ions.tpr
#genion_mpi -s ions.tpr -o 1AKI_solv_ions.gro -p topol.top -pname NA -nname CL -nn 8
# Before we can begin dynamics, we must ensure that the system has
# no steric clashes or inappropriate geometry. The structure is
# relaxed through a process called energy minimization (EM).
# create tpr file
# tpr file contains everything gromacs needs to run a sim
#grompp_mpi -f minim.mdp -c 1AKI_solv_ions.gro -p topol.top -o em.tpr
## invoke mdrun to carry out the EM:
#mpirun -n 12 mdrun_mpi -v -deffnm em
# output of previous step
# - em.log: ASCII-text log file of the EM process
# - em.edr: Binary energy file
# - em.trr: Binary full-precision trajectory
# - em.gro: Energy-minimized structure
##  NVT equilibriation
# constant Number of particles, Volume, and Temperature
#grompp_mpi -f nvt.mdp -c em.gro -p topol.top -o nvt.tpr
# about 10 min on 12 cores
mpirun -n 12 mdrun_mpi -v -deffnm nvt
# output of 
