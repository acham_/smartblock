#!/bin/bash
NLIST=`python makeNodeList.py nfile.txt`
numnodes=6
#NLIST=`hostlist --expand $RAW_NLIST`
#numnodes=`hostlist --count $RAW_NLIST`
echo "comma-separated node list: $NLIST"
echo "num nodes: $numnodes"

numwf=$((numnodes/2))
simwfmod=$((numnodes % 2))
nummaghist=$((numwf/3))
mod=$((numwf % 3))
numsel=$((nummaghist + mod))

echo "numwf $numwf simwfmod $simwfmod nummaghist $nummaghist numsel $numsel"

ix=0

comma_pos=`expr index $NLIST ","`


histls=""
magls=""
sells=""
newnode=""

# Distribute Nodes- Create host lists #

for i in `seq 1 $nummaghist`;
do
  if [ $ix -lt $((numwf-1)) ]
  then
    newnode=`expr substr $NLIST 1 $((comma_pos-1))`
    NLIST=${NLIST#"$newnode,"}
    histls+="$newnode"
    if [ $i -eq $nummaghist ]
    then
      let ix=ix+1
      break
    else
      histls+=","
    fi  
  else
    histls+=$NLIST
    break
  fi  
  let ix=ix+1
  comma_pos=`expr index $NLIST ","`
done
echo "hist node list: $histls with ix=$ix"

comma_pos=`expr index $NLIST ","`
for i in `seq 1 $nummaghist`;
do
  if [ $ix -lt $((numwf-1)) ] #last node not yet reached
  then
    newnode=`expr substr $NLIST 1 $((comma_pos-1))`
    NLIST=${NLIST#"$newnode,"}
    magls+="$newnode"
    if [ $i -eq $nummaghist ]
    then
      let ix=ix+1
      break
    else
      magls+=","
    fi
  else #last node reached
    magls+=$NLIST
    break
  fi
  let ix=ix+1
  comma_pos=`expr index $NLIST ","`
done
echo "magnitude node list: $magls with ix=$ix"

comma_pos=`expr index $NLIST ","`
for i in `seq 1 $numsel`;
do
  if [ $ix -lt $((numwf)) ] #last node not yet reached
  then
    newnode=`expr substr $NLIST 1 $((comma_pos-1))`
    NLIST=${NLIST#"$newnode,"}
    sells+="$newnode"
    if [ $i -eq $numsel ]
    then
      let ix=ix+1
      break
    else
      sells+=","
    fi
  else #last node reached
    sells+=$NLIST
    break
  fi
  let ix=ix+1
  comma_pos=`expr index $NLIST ","`
done
echo "select node list: $sells with ix=$ix"


simls=$NLIST
echo "sim node list: $NLIST with $((numnodes - ix)) nodes"

