#!/bin/bash

# FALCON
mpirun --oversubscribe -n 4 -H falcon2 lmp_falcon < in.cracktiny &> sim.out &
mpirun --oversubscribe -n 4 -H falcon3 lammps_analysis 16 &> analysis.out &

wait
